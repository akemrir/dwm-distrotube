#include <X11/XF86keysym.h>

/* See LICENSE file for copyright and license details. */
/* appearance */
static const unsigned int borderpx    = 4;        /* border pixel of windows */
static const unsigned int snap        = 32;       /* snap pixel */
static const unsigned int gappx       = 4;        /* pixel gap between clients */
static const int showbar              = 1;        /* 0 means no bar */
static const int topbar               = 1;        /* 0 means bottom bar */
static const int horizpadbar          = 6;        /* horizontal padding for statusbar */
static const int vertpadbar           = 7;        /* vertical padding for statusbar */
static const char *fonts[]            = {"Mononoki Nerd Font:size=12",
                     "Noto Sans Mono:size=12:antialias=true:autohint=true",
                     "Symbola:size=12:antialias=true:autohint=true",
                     "Monospace:size=12:antialias=true:autohint=true"
                    };
static const char dmenufont[]         = "Mononoki Nerd Font:size=9";
static const char col_gray1[]         = "#242731";
static const char col_gray2[]         = "#242731"; /* border color unfocused windows */
static const char col_gray3[]         = "#96b5b4";
static const char col_gray4[]         = "#ffffff";
static const char col_cyan[]          = "#5F875F"; /* border color focused windows and tags */
/* bar opacity
 * 0xff is no transparency.
 * 0xee adds wee bit of transparency.
 * Play with the value to get desired transparency.
 */
static const unsigned int baralpha    = 0xcc;
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3]        = {
  /*               fg         bg         border   */
  [SchemeNorm] = { col_gray4, col_gray1, col_gray2 },
  [SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};
static const unsigned int alphas[][3] = {
  /*               fg      bg        border     */
  [SchemeNorm] = { OPAQUE, baralpha, borderalpha },
  [SchemeSel]  = { OPAQUE, baralpha, borderalpha },
};

/* tagging */
/* static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" }; */
/* static const char *tags[] = { "", "", "", "", "", "", "", "", "" }; */
static const char *tags[] = { "", "", "", "", "", "", "", "", "" };

static const Rule rules[] = {
  /* xprop(1):
   *  WM_CLASS(STRING) = instance, class
   *  WM_NAME(STRING) = title
   */
  /* class             , instance , title         , tags mask , isfloating , monitor , */
  { NULL               , NULL     , "nested"      , 0         , 1          , -1 }    ,
  { NULL               , NULL     , "gscreenshot" , 0         , 1          , -1 }    ,
  { "Guake.py"         , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Exe"              , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "OnBoard"          , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Florence"         , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Plugin-container" , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Terminal"         , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Gpaint"           , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Kolourpaint"      , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Wrapper"          , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Grc-prompter"     , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Ghost"            , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "feh"              , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Gnuplot"          , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Pinta"            , NULL     , NULL          , 0         , 1          , -1 }    ,

  { "xzoom"            , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Wine"             , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Thunar"           , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Zeal"             , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Qalculate-gtk"    , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Xephyr"           , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Yad"              , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Gimp"             , NULL     , NULL          , 0         , 1          , -1 }    ,
  { "Firefox"          , NULL     , NULL          , 1 << 8    , 0          , -1 }    ,
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

#include "layouts.c"
static const Layout layouts[] = {
  /* symbol     arrange function */
  { "[]=",      tile },    /* first entry is default */
  { "><>",      NULL },    /* no layout function means floating behavior */
  { "[M]",      monocle },
  { "HHH",      grid },
  { NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
  { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
  { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
  { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
  { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
#define CMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-p", "Run: ", NULL };
static const char *termcmd[]  = { "st", NULL };

static Key keys[] = {
  /* modifier             key        function        argument */
  { MODKEY             , XK_p                    , spawn       , {.v = dmenucmd } }                                  ,
  { MODKEY|ShiftMask   , XK_p                    , spawn       , CMD("./bin/a-winactivate") }                        ,
  { MODKEY             , XK_Return               , spawn       , {.v = termcmd } }                                   ,
  { MODKEY             , XK_b                    , togglebar   , {0} }                                               ,
  { MODKEY|ShiftMask   , XK_b                    , spawn       , CMD("./bin/a-pa-switch") }                          ,
  { MODKEY|ShiftMask   , XK_j                    , rotatestack , {.i = +1 } }                                        ,
  { MODKEY|ShiftMask   , XK_k                    , rotatestack , {.i = -1 } }                                        ,
  { MODKEY             , XK_j                    , focusstack  , {.i = +1 } }                                        ,
  { MODKEY             , XK_k                    , focusstack  , {.i = -1 } }                                        ,
  { MODKEY             , XK_i                    , incnmaster  , {.i = +1 } }                                        ,
  { MODKEY             , XK_d                    , incnmaster  , {.i = -1 } }                                        ,
  { MODKEY             , XK_h                    , setmfact    , {.f = -0.05} }                                      ,
  { MODKEY             , XK_l                    , setmfact    , {.f = +0.05} }                                      ,
  { MODKEY|ControlMask , XK_Return               , zoom        , {0} }                                               ,
  { MODKEY             , XK_Tab                  , view        , {0} }                                               ,
  { MODKEY|ShiftMask   , XK_c                    , killclient  , {0} }                                               ,

  { MODKEY             , XK_z          , spawn       , CMD("xzoom") }               ,

  { MODKEY             , XK_bracketleft          , spawn       , CMD("./bin/dmenu-mpd -a add_album") }               ,
  { MODKEY|ShiftMask   , XK_bracketleft          , spawn       , CMD("./bin/dmenu-mpd -a play_album_song") }         ,
  { MODKEY             , XK_comma                , spawn       , CMD("./bin/dmenu-mpd -a open_album_dir") }          ,
  { MODKEY             , XK_period               , spawn       , CMD("./bin/zraz") }                                 ,
  { MODKEY             , XK_n                    , spawn       , CMD("./bin/dmenu-mpd -a now_playing") }             ,
  { MODKEY|ShiftMask   , XK_n                    , spawn       , CMD("./bin/dmenu-mpd -a now_playing_album_songs") } ,
  { 0                  , XF86XK_AudioMute        , spawn       , CMD("./bin/vol -m") }                               ,
  { 0                  , XF86XK_AudioRaiseVolume , spawn       , CMD("./bin/vol -u") }                               ,
  { 0                  , XF86XK_AudioLowerVolume , spawn       , CMD("./bin/vol -d") }                               ,
  { 0                  , XF86XK_AudioPrev        , spawn       , CMD("./bin/dmenu-mpd -a prev") }                    ,
  { 0                  , XF86XK_AudioNext        , spawn       , CMD("./bin/dmenu-mpd -a next") }                    ,
  { 0                  , XF86XK_AudioPause       , spawn       , CMD("./bin/dmenu-mpd -a pause") }                   ,
  { 0                  , XF86XK_AudioPlay        , spawn       , CMD("./bin/dmenu-mpd -a pause") }                   ,
  { 0                  , XF86XK_AudioStop        , spawn       , CMD("./bin/dmenu-mpd -a pause") }                   ,

  { MODKEY             , XK_v                    , spawn       , CMD("./bin/dmenu-record") }                         ,

  { 0                  , XK_Print                , spawn       , CMD("./bin/maimfull") }                             ,
  { ShiftMask          , XK_Print                , spawn       , CMD("./bin/maimpick") }                             ,
  /* { MODKEY,      XK_Print,  spawn,    CMD("dmenurecord") }, */
  /* { MODKEY|ShiftMask,    XK_Print,  spawn,    CMD("dmenurecord kill") }, */
  /* { MODKEY,      XK_Delete,  spawn,    CMD("dmenurecord kill") }, */
  /* { MODKEY,      XK_Scroll_Lock,  spawn,    SHCMD("killall screenkey || screenkey &") }, */

    /* Layout manipulation */
  { MODKEY           , XK_Tab   , cyclelayout    , {.i = -1 } } ,
  { MODKEY|ShiftMask , XK_Tab   , cyclelayout    , {.i = +1 } } ,
  { MODKEY|ShiftMask , XK_f     , togglefloating , {0} }        ,
  /* { MODKEY           , XK_space , setlayout      , {0} }        , */

    /* Switch to specific layouts */
  { MODKEY,               XK_t,      setlayout,      {.v = &layouts[0]} },
  { MODKEY,               XK_f,      setlayout,      {.v = &layouts[1]} },
  { MODKEY,               XK_m,      setlayout,      {.v = &layouts[2]} },
  { MODKEY,               XK_g,      setlayout,      {.v = &layouts[3]} },

  { MODKEY,               XK_0,      view,           {.ui = ~0 } },
  { MODKEY|ShiftMask,     XK_0,      tag,            {.ui = ~0 } },

  /* Switching between monitors */
  /* { MODKEY,               XK_comma,  focusmon,       {.i = -1 } }, */
  /* { MODKEY,               XK_period, focusmon,       {.i = +1 } }, */
  /* { MODKEY|ShiftMask,     XK_comma,  tagmon,         {.i = -1 } }, */
  /* { MODKEY|ShiftMask,     XK_period, tagmon,         {.i = +1 } }, */

    /* Dmenu scripts launched with ALT + CTRL + KEY */
  { Mod1Mask|ControlMask , XK_e , spawn , CMD("./.dmenu/dmenu-edit-configs.sh") } ,
  { Mod1Mask|ControlMask , XK_p , spawn , CMD("passmenu") }                       ,
  /* { Mod1Mask|ControlMask, XK_m,      spawn,          CMD("./.dmenu/dmenu-sysmon.sh") }, */
  /* { Mod1Mask|ControlMask, XK_r,      spawn,          CMD("./.dmenu/dmenu-reddio.sh") }, */
  /* { Mod1Mask|ControlMask, XK_s,      spawn,          CMD("./.dmenu/dmenu-surfraw.sh") }, */
  /* { Mod1Mask|ControlMask, XK_t,      spawn,          CMD("./.dmenu/dmenu-trading.sh") }, */
  /* { Mod1Mask|ControlMask, XK_i,      spawn,          CMD("./.dmenu/dmenu-scrot.sh") }, */

    /* Apps Launched with SUPER + ALT + KEY */
  /* { MODKEY|Mod1Mask,        XK_b,    spawn,          CMD("surf suckless.org") }, */
  /* { MODKEY|Mod1Mask,        XK_l,    spawn,          CMD("st -e lynx gopher://distro.tube") }, */
  /* { MODKEY|Mod1Mask,        XK_f,    spawn,          CMD("st -e vifm") }, */
  /* { MODKEY|Mod1Mask,        XK_i,    spawn,          CMD("st -e irssi") }, */
  /* { MODKEY|Mod1Mask,        XK_n,    spawn,          CMD("st -e newsboat") }, */
  /* { MODKEY|Mod1Mask,        XK_r,    spawn,          CMD("st -e rtv") }, */
  /* { MODKEY|Mod1Mask,        XK_e,    spawn,          CMD("st -e neomutt") }, */

  TAGKEYS(                  XK_1,          0)
  TAGKEYS(                  XK_2,          1)
  TAGKEYS(                  XK_3,          2)
  TAGKEYS(                  XK_4,          3)
  TAGKEYS(                  XK_5,          4)
  TAGKEYS(                  XK_6,          5)
  TAGKEYS(                  XK_7,          6)
  TAGKEYS(                  XK_8,          7)
  TAGKEYS(                  XK_9,          8)
  { MODKEY|ShiftMask,       XK_q,     quit,       {0} },
  { MODKEY|ShiftMask,       XK_r,    quit,           {1} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
  /* click           event mask   button          function        argument */
  { ClkLtSymbol,     0,           Button1,        setlayout,      {0} },
  { ClkLtSymbol,     0,           Button3,        setlayout,      {.v = &layouts[2]} },
  { ClkWinTitle,     0,           Button2,        zoom,           {0} },
  { ClkStatusText,   0,           Button2,        spawn,          {.v = termcmd } },
  { ClkClientWin,    MODKEY,      Button1,        movemouse,      {0} },
  { ClkClientWin,    MODKEY,      Button2,        togglefloating, {0} },
  { ClkClientWin,    MODKEY,      Button3,        resizemouse,    {0} },
  { ClkTagBar,       0,           Button1,        view,           {0} },
  { ClkTagBar,       0,           Button3,        toggleview,     {0} },
  { ClkTagBar,       MODKEY,      Button1,        tag,            {0} },
  { ClkTagBar,       MODKEY,      Button3,        toggletag,      {0} },
};

